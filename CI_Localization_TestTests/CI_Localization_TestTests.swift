//
//  CI_Localization_TestTests.swift
//  CI_Localization_TestTests
//
//  Created by Ryan Marshall on 8/2/18.
//  Copyright © 2018 Specialized Bicycle Components. All rights reserved.
//

import XCTest
@testable import CI_Localization_Test

class CI_Localization_TestTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
