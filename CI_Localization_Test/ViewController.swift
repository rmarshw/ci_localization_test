//
//  ViewController.swift
//  CI_Localization_Test
//
//  Created by Ryan Marshall on 8/2/18.
//  Copyright © 2018 Specialized Bicycle Components. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let label = UILabel()               //declaring variable of label type
        label.frame = CGRect(x:50, y:50, width:200, height:30)   //setting frame
        label.backgroundColor = UIColor.brown//setting backgroundColor
        label.textColor = UIColor.white     //setting text color
        label.textAlignment = NSTextAlignment.center //setting alignment of text to left in label
        label.text = NSLocalizedString("My First Label", comment:"the first label") //writing text on the label
        self.view.addSubview(label)                 //adding subView
        
        //Mark:- 2nd label ----->
        
        let label2 = UILabel()               //declaring variable of label type
        label2.frame = CGRect(x:50, y:100, width:250, height:40)  //setting frame
        label2.backgroundColor = UIColor.green  //setting backgroundColor
        label2.textColor = UIColor.purple     //setting text color
        label2.textAlignment = NSTextAlignment.center //setting alignment of text to right in label
        label2.text = NSLocalizedString("My Second Label", comment:"the second label") //writing text on the label
        self.view.addSubview(label2)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

